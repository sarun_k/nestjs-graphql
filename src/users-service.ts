import { Injectable } from '@nestjs/common';

@Injectable()
export class UserService {
  findCurrentUser() {
    console.log('find current user');
    return {
      id: 1,
      firstName: 'John',
      lastName: 'Doe',
    };
  }

  findById(id: number) {
    console.log(`find user by id[${id}]`);
    return {
      id,
      firstName: 'Jane',
      lastName: 'Doe',
    };
  }
}
