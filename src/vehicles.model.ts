import { Field, ID, ObjectType } from '@nestjs/graphql';
import { Coupon } from './coupons.model';

@ObjectType()
export class Vehicle {
  @Field(() => ID)
  id: number;

  @Field()
  licensePlate: string;

  @Field(() => [Coupon])
  coupons: [Coupon];
}
