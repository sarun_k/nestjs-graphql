import { Injectable } from '@nestjs/common';
import delay from 'delay';
import { Coupon } from './coupons.model';

@Injectable()
export class CouponService {
  async findByVehicleId(vehicleId: number): Promise<Coupon[]> {
    console.log(`begin find coupons by vehicleId[${vehicleId}]`);
    const coupon1 = new Coupon();
    coupon1.id = 1;
    coupon1.name = 'discount 15%';
    const coupon2 = new Coupon();
    coupon2.id = 2;
    coupon2.name = 'discount 10%';
    await delay(3000);
    console.log(`end find coupons by vehicleId[${vehicleId}]`);
    return [coupon1, coupon2];
  }
}
