import {
  Args,
  Int,
  Parent,
  Query,
  ResolveField,
  Resolver,
} from '@nestjs/graphql';
import { User } from './users.model';
import { UserService } from './users-service';
import { VehicleService } from './vehicle-service';

@Resolver(() => User)
export class UsersResolver {
  constructor(
    private userService: UserService,
    private vehicleService: VehicleService,
  ) {}

  @Query(() => User)
  async viewer() {
    return this.userService.findCurrentUser();
  }

  @Query(() => User)
  async user(@Args('id', { type: () => Int }) id: number) {
    return this.userService.findById(id);
  }

  @ResolveField()
  async vehicles(@Parent() user: User) {
    return this.vehicleService.findByUserId(user.id);
  }
}
