import { Field, ID, ObjectType } from '@nestjs/graphql';

@ObjectType()
export class Coupon {
  @Field(() => ID)
  id: number;

  @Field()
  name: string;
}
