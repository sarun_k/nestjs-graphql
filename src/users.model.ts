import { Field, ID, ObjectType } from '@nestjs/graphql';
import { Vehicle } from './vehicles.model';

@ObjectType()
export class User {
  @Field(() => ID)
  id: number;

  @Field()
  firstName: string;

  @Field()
  lastName: string;

  @Field(() => [Vehicle])
  vehicles: [Vehicle];
}
