import { Injectable } from '@nestjs/common';
import { Vehicle } from './vehicles.model';

@Injectable()
export class VehicleService {
  findByUserId(userId: number): Vehicle[] {
    console.log(`find vehicle by userId[${userId}]`);
    const vehicle1 = new Vehicle();
    vehicle1.id = 1;
    vehicle1.licensePlate = 'ABCDE';
    const vehicle2 = new Vehicle();
    vehicle2.id = 2;
    vehicle2.licensePlate = 'QRSTU';

    return [vehicle1, vehicle2];
  }
}
