import { Module } from '@nestjs/common';
import { ApolloDriver, ApolloDriverConfig } from '@nestjs/apollo';
import { GraphQLModule } from '@nestjs/graphql';
import { join } from 'path';
import { UsersResolver } from './users.resolver';
import { VehicleResolver } from './vehicles.resolver';
import { UserService } from './users-service';
import { VehicleService } from './vehicle-service';
import { CouponService } from './coupons-service';

@Module({
  imports: [
    GraphQLModule.forRoot<ApolloDriverConfig>({
      driver: ApolloDriver,
      autoSchemaFile: join(process.cwd(), 'src/schema.gql'),
    }),
  ],
  providers: [
    UsersResolver,
    VehicleResolver,
    UserService,
    VehicleService,
    CouponService,
  ],
})
export class AppModule {}
