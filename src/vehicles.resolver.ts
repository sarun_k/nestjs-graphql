import { Parent, ResolveField, Resolver } from '@nestjs/graphql';
import { CouponService } from './coupons-service';
import { Vehicle } from './vehicles.model';

@Resolver(() => Vehicle)
export class VehicleResolver {
  constructor(private couponService: CouponService) {}
  @ResolveField()
  async coupons(@Parent() vehicle: Vehicle) {
    return await this.couponService.findByVehicleId(vehicle.id);
  }
}
